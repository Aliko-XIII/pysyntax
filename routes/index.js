var express = require('express');
var router = express.Router();
const app = require('../app');
const path = require('path');
let publicPath = path.join(__dirname, 'public');

/* GET home page. */
router.get('/', (req, res) => {
  res.sendFile(`${publicPath}/index.html`);
  console.log(req);
})


module.exports = router;
