var createError = require('http-errors');
var express = require('express');
var path = require('path');
var cookieParser = require('cookie-parser');
var logger = require('morgan');

var indexRouter = require('./routes/index');
var usersRouter = require('./routes/users');
const { error } = require('console');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'views'));
app.set('view engine', 'jade');

app.use(logger('dev'));
app.use(express.json());
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);
app.use('/users', usersRouter);

app.use(express.urlencoded({ extended: false }));

const spawn = require("child_process").spawn;

app.use(express.text('utf-8'));
app.post('/code', (req, res) => {
  let code = req.body;
  let previous;
  do {
    previous = code;
    code = code.replace('<br>', '\n');
  }
  while (code != previous)
  const pythonProcess = spawn('python', ['./script.py', code]);
  pythonProcess.stdout.on('data', data => { res.send(data.toString()); });
});

module.exports = app;
