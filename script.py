import sys
import subprocess

python_code = sys.argv[1]

fname = 'generated.py'

with open(fname, 'w') as f:
    f.write(python_code)

try:
    test = subprocess.check_output(
        "pylint " + fname + " --output-format=text", shell=True)
except subprocess.CalledProcessError as e:
    test = e.output
print(str(test))
sys.stdout.flush()
