"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var codeBlock = document.getElementsByClassName('code-block')[0];
function createRowCount() {
    var rowCount = document.createElement('span');
    rowCount.role = 'textbox';
    rowCount.contentEditable = 'false';
    rowCount.className = 'row-count';
    codeBlock.appendChild(rowCount);
    return rowCount;
}
function createRowInput() {
    var input = document.createElement('span');
    input.role = 'textbox';
    input.contentEditable = 'true';
    input.className = 'row-input';
    input.innerText += '#input your code here';
    var inputWrap = document.createElement('div');
    inputWrap.className = 'input-wrap';
    inputWrap.appendChild(input);
    codeBlock.appendChild(inputWrap);
    return input;
}
var rowCount = createRowCount();
var input = createRowInput();
var hiddenInput = document.getElementsByName('input-hidden')[0];
rowCount.innerText = '1';
input.addEventListener('DOMSubtreeModified', function (event) {
    hiddenInput.setAttribute('value', input.innerText);
    input.innerText.trimStart();
    var count = 1;
    rowCount.innerText = '1';
    input.innerText.split('').forEach(function (char) {
        if (char == '\n') {
            if (count != 1)
                rowCount.innerText += "\n".concat(count);
            count++;
        }
    });
});
input.addEventListener("paste", function (event) {
    event.preventDefault();
    document.execCommand('inserttext', false, event.clipboardData.getData('text/plain'));
});
var form = document.getElementById('codeForm');
var submitBtn = document.getElementsByName('btn-submit')[0];
var errorsMessages = document.getElementsByClassName('errors-messages')[0];
form.addEventListener('submit', function (event) { event.preventDefault(); });
submitBtn.addEventListener('submit', function (event) { event.preventDefault(); });
submitBtn.addEventListener('click', function (event) {
    var formData = input.innerHTML;
    fetch('http://localhost:3000/code', {
        method: 'POST',
        body: formData,
        headers: {
            'Content-Type': 'text/plain',
        }
    })
        .then(function (response) { return response.text(); })
        .then(function (data) {
        var errors = data.toString();
        console.log(errors);
        var previous;
        do {
            previous = errors;
            errors = errors.replace('\\r', ' ');
            errors = errors.replace('\'', '');
        } while (errors != previous);
        var arr = errors.split('\\n');
        console.log(arr);
        arr.forEach(function (line) { errorsMessages.innerHTML += line + '<br>'; });
    })
        .catch(function (error) {
        console.error('Error:', error);
    });
});
