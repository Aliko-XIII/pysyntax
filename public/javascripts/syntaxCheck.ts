import { count } from "console";
import { add } from "nodemon/lib/rules";
const codeBlock: Element = document.getElementsByClassName('code-block')[0];

function createRowCount(): HTMLSpanElement {
    let rowCount: HTMLSpanElement = document.createElement('span');
    rowCount.role = 'textbox';
    rowCount.contentEditable = 'false';
    rowCount.className = 'row-count';
    codeBlock.appendChild(rowCount);
    return rowCount;
}

function createRowInput(): HTMLSpanElement {
    let input: HTMLSpanElement = document.createElement('span');
    input.role = 'textbox';
    input.contentEditable = 'true';
    input.className = 'row-input';
    input.innerText += '#input your code here';
    let inputWrap = document.createElement('div');
    inputWrap.className = 'input-wrap';
    inputWrap.appendChild(input);
    codeBlock.appendChild(inputWrap);
    return input;
}

const rowCount: HTMLSpanElement = createRowCount();
const input: HTMLSpanElement = createRowInput();
const hiddenInput: Element = document.getElementsByName('input-hidden')[0];

rowCount.innerText = '1';

input.addEventListener('DOMSubtreeModified', event => {
    hiddenInput.setAttribute('value', input.innerText);
    input.innerText.trimStart();
    let count = 1;
    rowCount.innerText = '1';
    input.innerText.split('').forEach(char => {
        if (char == '\n') {
            if (count != 1) rowCount.innerText += `\n${count}`
            count++;
        }
    });
});

input.addEventListener("paste", event => {
    event.preventDefault();
    document.execCommand('inserttext', false, event.clipboardData.getData('text/plain'));
});

const form = document.getElementById('codeForm') as HTMLFormElement;
const submitBtn = document.getElementsByName('btn-submit')[0] as HTMLButtonElement;
const errorsMessages = document.getElementsByClassName('errors-messages')[0] as HTMLDivElement;

form.addEventListener('submit', event => { event.preventDefault(); });
submitBtn.addEventListener('submit', event => { event.preventDefault(); });

submitBtn.addEventListener('click', event => {
    var formData = input.innerHTML;
    fetch('http://localhost:3000/code', {
        method: 'POST',
        body: formData,
        headers: {
            'Content-Type': 'text/plain',
        }
    })
        .then(response => response.text())
        .then(data => {
            let errors: string = data.toString();
            console.log(errors);
            let previous: string;
            do {
                previous = errors;
                errors = errors.replace('\\r', ' ');
                errors = errors.replace('\'', '');
            }
            while (errors != previous)
            let arr = errors.split('\\n');
            console.log(arr);
            arr.forEach(line => { errorsMessages.innerHTML += line + '<br>'; });

        })
        .catch(error => {
            console.error('Error:', error);
        });
})


